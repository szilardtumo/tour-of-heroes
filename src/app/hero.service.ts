import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of, throwError } from 'rxjs';
import { catchError, filter, map, tap } from 'rxjs/operators';
import filterNullish from 'src/operators/filterNull.operator';

import { Hero } from './hero';
import { MessageService } from './message.service';
import { heroes } from './mock-heroes';

@Injectable({
  providedIn: 'root'
})
export class HeroService {

  constructor(private httpClient: HttpClient, private messageService: MessageService) { }

  getHeroes(): Observable<Hero[]> {
    return this.httpClient
      .get<Hero[]>('api/heroes')
      .pipe(
        tap(() => this.messageService.add('Fetched heroes.')),
        catchError(this.handleError<Hero[]>('getHeroes', [])),
      );
  }

  searchHeroes(term: string): Observable<Hero[]> {
    return this.httpClient
      .get<Hero[]>(`api/heroes?name=${term}`)
      .pipe(
        tap(() => this.messageService.add(`Fetched heroes with name ${term}.`)),
        catchError(this.handleError<Hero[]>('searchHeroes', [])),
      );
  }

  getHero(id: number): Observable<Hero> {
    return this.httpClient
      .get<Hero>(`api/heroes/${id}`)
      .pipe(
        tap(() => this.messageService.add(`Fetched hero ${id}.`)),
        catchError(this.handleError<Hero>('getHero')),
      );
  }

  updateHero(hero: Hero): Observable<Hero> {
    return this.httpClient
      .put<Hero>('api/heroes', hero, { headers: new HttpHeaders({ 'Content-Type': 'application/json' }) })
      .pipe(
        tap(() => this.messageService.add(`Updated hero ${hero.id}.`)),
        map(() => hero),
        catchError(this.handleError<Hero>('updateHero')),
      );
  }

  createHero(name: string): Observable<Hero> {
    return this.httpClient
      .post<Hero>('api/heroes', { name }, { headers: new HttpHeaders({ 'Content-Type': 'application/json' }) })
      .pipe(
        tap((hero) => this.messageService.add(`Created hero ${hero.id}.`)),
        catchError(this.handleError<Hero>('createHero')),
      );
  }

  deleteHero(id: number): Observable<void> {
    return this.httpClient
      .delete<void>(`api/heroes/${id}`)
      .pipe(
        tap(() => this.messageService.add(`Deleted hero ${id}.`)),
        catchError(this.handleError<void>('deleteHero')),
      );
  }

  handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.error(error);
      this.messageService.add(`${operation} failed: ${error.message}`);

      return of(result).pipe(filterNullish());
    }
  }
}
