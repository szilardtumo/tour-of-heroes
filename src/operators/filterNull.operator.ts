import { Observable } from "rxjs";
import { filter } from "rxjs/operators";

export default function filterNullish() {
    return <T>(source: Observable<T | null | undefined>): Observable<T> =>
        source.pipe(
            filter((item): item is T => item !== null && item !== undefined)
        );
}